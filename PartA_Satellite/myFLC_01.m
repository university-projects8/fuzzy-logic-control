function [flc] = myFLC_01()
    %% General 
    flc = mamfis;
    
    %LARSEN: Implication Method with product
    flc.ImplicationMethod="prod";
    flc.AndMethod="min";
    flc.OrMethod="max";
    flc.AggregationMethod="max";
   
    % Inputs-Output
    flc = addInput(flc,[-1 1],'Name', 'e');
    flc = addInput(flc,[-1 1],'Name', 'edot' );
    flc = addOutput(flc,[-1 1],'Name', 'udot');

    %% Fuzzy sets for input e
    flc = addmf(flc, 'input', 1, 'NV', 'trimf', [-1-3/8 -1 -1+3/8]);
    flc = addmf(flc, 'input', 1, 'NL', 'trimf', [-0.75-3/8 -0.75 -0.75+3/8]);
    flc = addmf(flc, 'input', 1, 'NM', 'trimf', [-0.5-3/8 -0.5 -0.5+3/8]);
    flc = addmf(flc, 'input', 1, 'NS', 'trimf', [-0.25-3/8 -0.25 -0.25+3/8]);
    flc = addmf(flc, 'input', 1, 'ZR', 'trimf', [-3/8 0 3/8]);
    flc = addmf(flc, 'input', 1, 'PS', 'trimf', [0.25-3/8 0.25 0.25+3/8]);
    flc = addmf(flc, 'input', 1, 'PM', 'trimf', [0.5-3/8 0.5 0.5+3/8]);
    flc = addmf(flc, 'input', 1, 'PL', 'trimf', [0.75-3/8 0.75 0.75+3/8]);
    flc = addmf(flc, 'input', 1, 'PV', 'trimf', [1-3/8 1 1+3/8]);

    %% Fuzzy sets for input edot
    flc = addmf(flc, 'input', 2, 'NV', 'trimf', [-1-3/8 -1 -1+3/8]);
    flc = addmf(flc, 'input', 2, 'NL', 'trimf', [-0.75-3/8 -0.75 -0.75+3/8]);
    flc = addmf(flc, 'input', 2, 'NM', 'trimf', [-0.5-3/8 -0.5 -0.5+3/8]);
    flc = addmf(flc, 'input', 2, 'NS', 'trimf', [-0.25-3/8 -0.25 -0.25+3/8]);
    flc = addmf(flc, 'input', 2, 'ZR', 'trimf', [-3/8 0 3/8]);
    flc = addmf(flc, 'input', 2, 'PS', 'trimf', [0.25-3/8 0.25 0.25+3/8]);
    flc = addmf(flc, 'input', 2, 'PM', 'trimf', [0.5-3/8 0.5 0.5+3/8]);
    flc = addmf(flc, 'input', 2, 'PL', 'trimf', [0.75-3/8 0.75 0.75+3/8]);
    flc = addmf(flc, 'input', 2, 'PV', 'trimf', [1-3/8 1 1+3/8]);
    
    %% Fuzzy sets for output udot
    flc = addmf(flc, 'output', 1, 'NL', 'trimf', [-3/2 -1 -1/2]);
    flc = addmf(flc, 'output', 1, 'NM', 'trimf', [-2/3-1/2 -2/3 -2/3+1/2]);
    flc = addmf(flc, 'output', 1, 'NS', 'trimf', [-1/3-1/2 -1/3 -1/3+1/2]);
    flc = addmf(flc, 'output', 1, 'ZR', 'trimf', [-1/2 0 1/2]);
    flc = addmf(flc, 'output', 1, 'PS', 'trimf', [1/3-1/2 1/3 1/3+1/2]);
    flc = addmf(flc, 'output', 1, 'PM', 'trimf', [2/3-1/2 2/3 2/3+1/2]);
    flc = addmf(flc, 'output', 1, 'PL', 'trimf', [1/2 1 3/2]);
    
    
     rules=[1 9 4 1 1;
            1 8 3 1 1;
            1 7 2 1 1;
            1 6 1 1 1;
            1 5 1 1 1;
            1 4 1 1 1;
            1 3 1 1 1;
            1 2 1 1 1;
            1 1 1 1 1;
            2 9 5 1 1;
            2 8 4 1 1;
            2 7 3 1 1;
            2 6 2 1 1;
            2 5 1 1 1;
            2 4 1 1 1;
            2 3 1 1 1;
            2 2 1 1 1;
            2 1 1 1 1;
            3 9 6 1 1;
            3 8 5 1 1;
            3 7 4 1 1;
            3 6 3 1 1;
            3 5 2 1 1;
            3 4 1 1 1;
            3 3 1 1 1;
            3 2 1 1 1;
            3 1 1 1 1;
            4 9 7 1 1;
            4 8 6 1 1;
            4 7 5 1 1;
            4 6 4 1 1;
            4 5 3 1 1;
            4 4 2 1 1;
            4 3 1 1 1;
            4 2 1 1 1;
            4 1 1 1 1;
            5 9 7 1 1;
            5 8 7 1 1;
            5 7 6 1 1;
            5 6 5 1 1;
            5 5 4 1 1;
            5 4 3 1 1;
            5 3 2 1 1;
            5 2 1 1 1;
            5 1 1 1 1;
            6 9 7 1 1;
            6 8 7 1 1;
            6 7 7 1 1;
            6 6 6 1 1;
            6 5 5 1 1;
            6 4 4 1 1;
            6 3 3 1 1;
            6 2 2 1 1;
            6 1 1 1 1;
            7 9 7 1 1;
            7 8 7 1 1;
            7 7 7 1 1;
            7 6 7 1 1;
            7 5 6 1 1;
            7 4 5 1 1;
            7 3 4 1 1;
            7 2 3 1 1;
            7 1 2 1 1;
            8 9 7 1 1;
            8 8 7 1 1;
            8 7 7 1 1;
            8 6 7 1 1;
            8 5 7 1 1;
            8 4 6 1 1;
            8 3 5 1 1;
            8 2 4 1 1;
            8 1 3 1 1;
            9 9 7 1 1;
            9 8 7 1 1;
            9 7 7 1 1;
            9 6 7 1 1;
            9 5 7 1 1;
            9 4 7 1 1;
            9 3 6 1 1;
            9 2 5 1 1;
            9 1 4 1 1;];

    flc = addRule(flc, rules);

end
