close all;
clear all;

%% Linear PI Controller
% Design of the linear controller. Firstly, implement the plant and create the
% root locus plot based  on the openloop system. Then iteratively 
G_c=tf([1,2],[1,0]);
G_p=tf(10,[1 9 10]);

openloop=series(G_c,G_p);

% Root Locus of Open Loop System
% figure()
% rlocus(openloop)

% Setting unit gain doesn't meet our requirements
closedloop=feedback(0.1*openloop,1,-1);
figure()
step(closedloop)
S=stepinfo(closedloop);
RT=S.RiseTime;
OS=S.Overshoot;
Peak=S.Peak;
PT=S.PeakTime;
hold on
text(2,0.3,sprintf("RiseTime=%.2f sec",RT))
text(2,0.2,sprintf("OverShoot=%.2f %%",OS))

% * Iterative Process for Gain Selection: Gradually increase K * %

% Setting K=8, as it is we see that requirements are met
closedloop=feedback(0.8*openloop,1,-1);
%As a consequence Kp=K/10=0.8 and Ki=2*Kp=1.6 (@pole at -2)
figure()
step(closedloop)
S=stepinfo(closedloop);
RT=S.RiseTime;
OS=S.Overshoot;
Peak=S.Peak;
PT=S.PeakTime;
hold on
text(2,0.3,sprintf("RiseTime=%.2f sec",RT))
text(2,0.2,sprintf("OverShoot=%.2f %%",OS))
text(PT,1.1,'$\downarrow$','Interpreter','latex')
text(PT-0.3,1.15,sprintf("Peak=%.2f",Peak))

%% Fuzzy PI Controller
flc_system=myFLC_01;

% % Membership Functions of Inputs
figure()
plotmf(flc_system, 'input',1);
title('Membership function of e');

figure()
plotmf(flc_system, 'input',2);
title('Membership function of edot');

% Membership Functions of Output
figure()
plotmf(flc_system, 'output',1);
title('Membership function of udot');

%% The Scenarios for the Controllers are implemented in Simulink
