classdef Vehicle
   properties
      pos %of type Point
      theta {mustBeNumeric}
   end %properties
   methods (Static)
      function obj = Vehicle(point0,theta0)
         assert(isa(point0,'Point'), ...
            'Individual Constructor Error:  Coordinates of Position should be a Point obj instead of class %s.',class(point0));
         assert(abs(theta0)<=180, ...
            'Individual Constructor Error:  theta should be in range of [-180,180] deg');
         obj.pos = point0;
         obj.theta = theta0;
         return
      end

      function dv = get_sensors_dv(obj)
         switch true
            case obj.pos.x<=5
               dv=obj.pos.y-0;
            case obj.pos.x<=6
               dv=obj.pos.y-1;
            case obj.pos.x<=7
               dv=obj.pos.y-2;
            otherwise
               dv=obj.pos.y-3;
         end
         dv=min(dv,1); %dv shall be in range [0,1]
      end
      function dh = get_sensors_dh(obj)
         switch true
            case obj.pos.y<=1
               dh=5-obj.pos.x;
            case obj.pos.y<=2
               dh=6-obj.pos.x;
            case obj.pos.y<=3
               dh=7-obj.pos.x;
            otherwise
               dh=1; % In the horizontal plane there is no obstacle, so dh=1 max value
         end
         dh=min(dh,1); %dh shall be in range [0,1]
      end
   end %methods
end %classdef


