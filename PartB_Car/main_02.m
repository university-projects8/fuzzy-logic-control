%% FUZZY SYSTEMS Project 02 Part 1, August 2020
%  Author: Jason George Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

% Other files:
% --- Point: Object
% --- Vehicle: Object
% --- myFLC(): the Fuzzy Logic Controller to be implemented

%% Clear Vars and Close Figs
format compact
clear all;
close all;
dir=[pwd,'\images'];
mkdir(dir)

%% Initializations
% Initial Position of Vehicle and Initial Velocity provided in the Assignment
x_init=3.1;
y_init=0.3;
theta_init=[0,-45,-90];

% Create Vehicle Objects (one per different initial orientation)
car(1)=Vehicle(Point(x_init,y_init),theta_init(1));
car(2)=Vehicle(Point(x_init,y_init),theta_init(2));
car(3)=Vehicle(Point(x_init,y_init),theta_init(3));

% Linear Velocity which is kept constant through the movement
global u; u=0.05;

% Desirable Final Position provided by the Assignment
    xd=10;
    yd=3.1;
    
% Obstacles to be plotted and of course avoided.
    obstacle_x = [5; 5; 6; 6; 7; 7; 10];
    obstacle_y = [0; 1; 1; 2; 2; 3; 3];
    
%% Select Original or Enhanced FLC
% Create FLC: If you want the enhanced controller, comment the next line and uncomment the
% line after that

%flc_system=myFLC(0);
flc_system=myFLC(1);

%% Plot Membership Functions of Inputs
figure()
plotmf(flc_system, 'input',1);
title('Membership function of dh');
saveas(gcf,fullfile(dir,['MF_of_dh.png']))

figure()
plotmf(flc_system, 'input',2);
title('Membership function of dv');
saveas(gcf,fullfile(dir,['MF_of_dv.png']))

figure()
plotmf(flc_system, 'input',3);
title('Membership function of theta');
saveas(gcf,fullfile(dir,['MF_of_theta.png']))

% Membership Functions of Output
figure()
plotmf(flc_system, 'output',1);
title('Membership function of Dtheta');
saveas(gcf,fullfile(dir,['MF_of_dtheta.png']))

%% Main Loop of Movement of each Vehicle
for i=1:3
   
   position=car(i).pos;
   
   ctr=1; % Counter
   
   xnew=car(i).pos.x;
   
   while (xnew-xd<0) 
      
      % Read Sensor <Measurements
      dh = car(i).get_sensors_dh(car(i)); 
      dv = car(i).get_sensors_dv(car(i)); 
      
      % Debugging for errors to stop simulation
      if or(dh<=0,dh>1)
         fprintf("\nObstacle Hit\n")
         break;
      end
      if or(dv<=0,dv>1)
         fprintf("\nObstacle Hit\n")
         break;
      end
      
      % Evaluate input and calculate output based on flc
      Dtheta = evalfis([dh dv car(i).theta;], flc_system);

      % Update our vehicle (saturate theta angle in +-180)
      car(i).theta=car(i).theta+Dtheta;
      % Calculate that the movement might be over 360 degs so we need to accumulate for it
      
      if (car(i).theta<-180)
         car(i).theta=car(i).theta+360;
      elseif (car(i).theta>180)
         car(i).theta=car(i).theta-360;
      end

      % By definition of theta, we can propagate x and y based on cos and sine (in degs)
      xnew=car(i).pos.x+u*cosd(car(i).theta);
      ynew=car(i).pos.y+u*sind(car(i).theta);

      % Propagate the movement except the last one and store it.
      if xnew-xd<0
         car(i).pos= Point(xnew,ynew);
         position=[position car(i).pos];
      end
      
      ctr=ctr+1;
   end
   %% Plots
   % At the end of the movement store x and y separately (for plot reasons)
   for j=1:size(position,2)
       x(j)=position(j).x;
       y(j)=position(j).y;
   end   
    % Plot the movement
    figure()
    plot(x, y, '--','LineWidth',0.75);
    hold on;
    plot(obstacle_x, obstacle_y, 'r','LineWidth',2);
    % Mark the initial and desired points on the plot
    axis([2 11 0 4])
    plot(xd, yd, '*');
    plot(x_init, y_init, '*');
    text(xd-2,yd+0.5,sprintf('errorX=%.3f=%.2f%%',abs(xd-x(end)), abs(xd-x(end))*100/xd))
    text(xd-2,yd+0.3,sprintf('errorY=%.3f=%.2f%%',abs(yd-y(end)), abs(yd-y(end))*100/yd))
    saveas(gcf,fullfile(dir,['Movement_',num2str(i),'.png']))

end
clear dv dh i obstacle_x obstacle_y xd yd j
clear x_init y_init theta_init xnew ynew x y position theta u Dtheta ctr
