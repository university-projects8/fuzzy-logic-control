%% FUZZY SYSTEMS Project 04 Part B, August 2020

%  Author: Iason Georgios Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

% Other Files:
% --- split_scale.m: provided during the course
% --- train.csv: dataset to be used
% (Optionally) ranking.mat: in order to avoid running relieff algorithm again
%          and split.mat: to use the exact same division of datasets as in the report
% --- FIS_build_2: in order to manually build the FISs (2nd version to include more
% classes than 2)

%% Clear vars, Close figures
format compact
clear all;
close all;
clc
dir=[pwd,'\images2'];
mkdir(dir)

%% Load Data and Preprocess it 
data = unique(table2array(readtable('epilleptic.csv')),'rows');
m=size(data,2); %Number of Characteristics with the 82nd being the identifier.

%We need to check the proportion of each class identifier in each subset:
class=unique(data(:,end));
% CLASSES = 1,2,3,4,5
% While studying the data we observe that each class contains exactly 2300 instances.
% So, we will use this to perfectly balance the subsets (training-check-test).
% In that case firstly let's standardize the data using split_scale  and then we will split
% them on our own by recreating the dataset :)

% [trnData,chkData,tstData]=split_scale(data,1);
% data=[trnData;chkData;tstData];% Now data is just scaled
% 
% % Now, split them class-dependently ;)
% trnData=[]; chkData=[]; tstData=[];
% for i=1:length(class)
%    x=data(data(:,end)==i,:);
%    trnData=[trnData; x(1:round(0.6*size(x,1)),:)];
%    chkData=[chkData; x(round(0.6*size(x,1))+1:round(0.8*size(x,1)),:)];
%    tstData=[tstData; x(round(0.8*size(x,1))+1:end,:)];
% end

% %Save the partitioning for traceability/validation reasons
% save('partitioning2.mat','trnData','chkData','tstData')

% If the same partitioning with the deliverable needs to be tested uncomment next line
% and comment the previous 4 lines

load partitioning2.mat
% 
% [ranking, ~] = relieff(data(:,1:end-1), data(:,end), 100);
% save('ranking.mat','ranking')
load ranking.mat


model=[];

%% Initialize Grid Search Nodes
% The following lines have been adjusted iteratively, so the values differ. A total set is
% provided, but the user is encouraged to run FISs for fewer nodes, by creating similar
% vectors. "i-th value of the radius vector refers to i-th value of the features vector."


% run 1: [4 4 4 8 8 8 12 12 12 16 16 16]
% run 1: [0.3 0.5 0.7 0.3 0.5 0.7 0.3 0.5 0.7 0.3 0.5 0.7]
% run 2: [ 20 20  20 24 24 24 28 28 28 32 32 32];
% run 2: [ 0.3 0.5 0.7 0.3 0.5 0.7 0.3 0.5 0.7 0.3 0.5 0.7 ];
% run 3: [10 10 10 14 14 14 18 18 18];
% run 3: [ 0.6 0.8 0.9 0.6 0.8 0.9 0.6 0.8 0.9];
features = [10 10 12 12];
radius = [0.4 0.6 0.4 0.6]
%% Main FIS Training
for i=1:length(features)
    
    f=features(i); 
    r=radius(i);
   
   %Create a partitioning of the Training Data for 5-fold cross validation
   partition = cvpartition(trnData(:, end), 'KFold', 5);
    
   % Split data based on class (1 to 5), calculate centers and influence of each feature
   x=trnData(trnData(:,end)==1,:);     [c1,sig1]=subclust(x(:,[ranking(1:f),m]),r);
   x=trnData(trnData(:,end)==2,:);     [c2,sig2]=subclust(x(:,[ranking(1:f),m]),r);
   x=trnData(trnData(:,end)==3,:);     [c3,sig3]=subclust(x(:,[ranking(1:f),m]),r);
   x=trnData(trnData(:,end)==4,:);     [c4,sig4]=subclust(x(:,[ranking(1:f),m]),r);
   x=trnData(trnData(:,end)==5,:);     [c5,sig5]=subclust(x(:,[ranking(1:f),m]),r);
   
   % Build FIS to be trained manually
   fis=FIS_build_2(f+1,c1,c2,c3,c4,c5,sig1,sig2,sig3,sig4,sig5);
   eM=zeros(5,5,5);
   my_model = struct('Radius',r,'Features',f);
    OA=zeros(5,1);
    meanTT=-1;
    if length(fis.Rule)<2
       my_model.('State')='Only 1 Rule';
    elseif length(fis.Rule)>150
       my_model.('State')='Over 150 Rules';
    else
       my_model.('State')='Accepted Model';
       meanTT=0;
       for k=1:5
          disp(['Model: ',num2str(f),' features, ',num2str(r),' radius, ', 'Fold ',num2str(k)])
          tstart=tic;  % Start calculating Training Time
          % Train the FIS      
          [trnFis,trnError,~,valFis,valError] = anfis(trnData(partition.training(k),[ranking(1:f),m]), ...
             fis,[400 0 0.01 0.9 1.1],[],trnData(partition.test(k),[ranking(1:f),m])); 
          % Calculate mean Training Time
          meanTT=meanTT+toc(tstart)/5;
          % Estimate the output of valFis on chkData
          tstY_hat=evalfis(valFis,tstData(:,ranking(1:f)));
          % Discretize the output on the 5 classes
          for l=1:size(tstData,1)
             if tstY_hat(l)<1.5
                tstY_hat(l)=1;
             elseif tstY_hat(l)<2.5
                tstY_hat(l)=2;                
             elseif tstY_hat(l)<3.5
                tstY_hat(l)=3;                
             elseif tstY_hat(l)<4.5
                tstY_hat(l)=4;                
             else
                tstY_hat(l)=5;                                
             end
          end
          % Calculate overall Accuracy
          for l=1:size(tstData,1)
             eM(tstY_hat(l),tstData(l,end),k)= eM(tstY_hat(l),tstData(l,end),k)+1;
          end
          OA(k)=sum(diag(eM(:,:,k)))/size(tstData,1);
       end
    end
       my_model.('ErrorMatrix')=sum(eM(:,:,k),3)/5;
       my_model.('meanTT')=meanTT;
       overalAcc=sum(OA)/5;
       my_model.('overallAccuracy')=overalAcc;
       my_model.('num_Rules')=length(fis.Rule);
    % Store model
    model = [model my_model];
end
