
%% FUZZY SYSTEMS Project 04 Part B Final Model, August 2020

%  Author: Iason Georgios Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

% Other Files:
% --- split_scale.m: provided during the course
% --- train.csv: dataset to be used
% (Optionally) ranking.mat: in order to avoid running relieff algorithm again
%          and split.mat: to use the exact same division of datasets as in the report
% --- FIS_build_2: in order to manually build the FISs (2nd version to include more
% classes than 2)

%% Clear vars, Close figures
format compact
clear all;
close all;
clc
dir=[pwd,'\images2'];
mkdir(dir)

%% Load Data and Preprocess it 
data = unique(table2array(readtable('epilleptic.csv')),'rows');
m=size(data,2); %Number of Characteristics with the 82nd being the identifier.

%We need to check the proportion of each class identifier in each subset:
class=unique(data(:,end));
% CLASSES = 1,2,3,4,5
% While studying the data we observe that each class contains exactly 2300 instances.
% So, we will use this to perfectly balance the subsets (training-check-test).
% In that case firstly let's standardize the data using split_scale  and then we will split
% them on our own by recreating the dataset :)

% [trnData,chkData,tstData]=split_scale(data,1);
% data=[trnData;chkData;tstData];% Now data is just scaled
% 
% % Now, split them class-dependently ;)
% trnData=[]; chkData=[]; tstData=[];
% for i=1:length(class)
%    x=data(data(:,end)==i,:);
%    trnData=[trnData; x(1:round(0.6*size(x,1)),:)];
%    chkData=[chkData; x(round(0.6*size(x,1))+1:round(0.8*size(x,1)),:)];
%    tstData=[tstData; x(round(0.8*size(x,1))+1:end,:)];
% end

% %Save the partitioning for traceability/validation reasons
% save('partitioning2.mat','trnData','chkData','tstData')

% If the same partitioning with the deliverable needs to be tested uncomment next line
% and comment the previous 4 lines

load partitioning2.mat
% 
% [ranking, ~] = relieff(data(:,1:end-1), data(:,end), 100);
% save('ranking.mat','ranking')
load ranking.mat

f=12; r=0.7;
% Split data based on class (1 to 5), calculate centers and influence of each feature
x=trnData(trnData(:,end)==1,:);     [c1,sig1]=subclust(x(:,[ranking(1:f),m]),r);
x=trnData(trnData(:,end)==2,:);     [c2,sig2]=subclust(x(:,[ranking(1:f),m]),r);
x=trnData(trnData(:,end)==3,:);     [c3,sig3]=subclust(x(:,[ranking(1:f),m]),r);
x=trnData(trnData(:,end)==4,:);     [c4,sig4]=subclust(x(:,[ranking(1:f),m]),r);
x=trnData(trnData(:,end)==5,:);     [c5,sig5]=subclust(x(:,[ranking(1:f),m]),r);
% Build FIS to be trained manually
fis=FIS_build_2(f+1,c1,c2,c3,c4,c5,sig1,sig2,sig3,sig4,sig5);
model = struct('Radius',r,'Features',f,'Type','Class-Dependent');
% Train the FIS      
[trnFis,trnError,~,valFis,valError] = anfis(trnData(:,[ranking(1:f),m]), ...
    fis,[400 0 0.01 0.9 1.1],[],chkData(:,[ranking(1:f),m])); 
 
% Plot Learning Curve
figure()
plot([trnError,valError],'LineWidth',1);
grid on;
xlabel('# of Iterations'); ylabel('Error');
legend('Training Error','Validation Error');
title('Learning Curve');
saveas(gcf,fullfile(dir,['Final',num2str(r),'_learningcurve.png']))

tstY_hat=evalfis(valFis,tstData(:,ranking(1:f)));
% Plot actual classes vs predicted classes
figure()
plot(1:size(tstData,1),tstData(:,end),'ok',1:size(tstData,1),tstY_hat, '.r');
title('Estimation Error');
legend('Reference Outputs','Model Outputs');
saveas(gcf,fullfile(dir,'Instances_prev.png'))

for l=1:size(tstData,1)
    if tstY_hat(l)<1.5
          tstY_hat(l)=1;
    elseif tstY_hat(l)<2.5
          tstY_hat(l)=2;                
    elseif tstY_hat(l)<3.5
          tstY_hat(l)=3;                
    elseif tstY_hat(l)<4.5
          tstY_hat(l)=4;                
    else
          tstY_hat(l)=5;                                
    end
end
% Plot actual classes vs predicted classes
figure()
plot(1:size(tstData,1),tstData(:,end),'ok',1:size(tstData,1),tstY_hat, '.r');
title('Estimation Error');
legend('Reference Outputs','Model Outputs');
saveas(gcf,fullfile(dir,'Instances.png'))



% Plot some fuzzy sets
for i=[1,4,8,12]   
      figure() 
      subplot(2,1,1) 
      [x,mf] = plotmf(fis,'input',i); 
      plot(x,mf)
      title(['Initial MFs for Feature-Input ',num2str(i)])
      subplot(2,1,2) 
      [x,mf] = plotmf(valFis,'input',i); 
      plot(x,mf)
      title(['Tuned MFs for Feature-Input ',num2str(i)])
      saveas(gcf,fullfile(dir,['Model_','Final','_CompMfs_',num2str(i),'.png']))
end

% Calculate overall Accuracy
eM=zeros(5,5);
for l=1:size(tstData,1)
   eM(tstY_hat(l),tstData(l,end))= eM(tstY_hat(l),tstData(l,end))+1;
end
OA=sum(diag(eM(:,:)))/size(tstData,1);
model.('Num_Rules')=length(valFis.Rule);
model.('ErrorMatrix')=eM;
model.('OverallAcc')=sum(diag(eM))/size(tstData,1); 
model.('UserAcc')=diag(eM)./sum(eM,2);
model.('ProducerAcc')=diag(eM)'./sum(eM,1);
model.('k_hat')=(size(tstData,1)*sum(diag(eM))-sum(eM,1)*sum(eM,2))/(size(tstData,1)^2-sum(eM,1)*sum(eM,2));
 
   fprintf("Model evaluation\n")
   fprintf("Type: %s\n",model.Type)
   fprintf("Radius: %.2f\n",model.Radius);
   fprintf("Number of Rules: %d\n",model.Num_Rules)
   disp('Error Matrix: ')
   disp(model.ErrorMatrix)
   disp('Overall Accuracy: ')
   disp(model.OverallAcc)
   disp('User Accuracy: ')
   disp(model.UserAcc)
   disp('Producer Accuracy: ')
   disp(model.ProducerAcc)
   disp('Kappa Coefficient: ')
   disp(model.k_hat)
   fprintf("------------------------------------------------\n");
 
 
 
