function fis = FIS_build_2(m,c1,c2,c3,c4,c5,sig1,sig2,sig3,sig4,sig5)
   % Total number of rules defined of the number of clusters created
   n1=size(c1,1);    n2=size(c2,1);   n3=size(c3,1);    n4=size(c4,1);     n5=size(c5,1);
   num_rules=n1+n2+n3+n4+n5;
   %Build FIS From Scratch
   fis=newfis('FIS_SC','sugeno');
   %Add Input-Output Variables, we have 3 Inputs and 1 Output
   for i=1:m-1
       fis=addvar(fis,'input',['in_',num2str(i)],[0 1]);
   end
   fis=addvar(fis,'output','Output1',[1 2]);
   %Add Input Membership Functions
   for i=1:m-1
       for j=1:n1
           name=['mf_',num2str((num_rules)*(i-1)+j)];
           fis=addmf(fis,'input',i,name,'gaussmf',[sig1(i) c1(j,i)]);
       end
       for j=1:n2
           name=['mf_',num2str((num_rules)*(i-1)+j+n1)];
           fis=addmf(fis,'input',i,name,'gaussmf',[sig2(i) c2(j,i)]);
       end
       for j=1:n3
           name=['mf_',num2str((num_rules)*(i-1)+j+n1+n2)];
           fis=addmf(fis,'input',i,name,'gaussmf',[sig3(i) c3(j,i)]);
       end
       for j=1:n4
        name=['mf_',num2str((num_rules)*(i-1)+j+n1+n2+n3)];
        fis=addmf(fis,'input',i,name,'gaussmf',[sig4(i) c4(j,i)]);
       end
       for j=1:n5
        name=['mf_',num2str((num_rules)*(i-1)+j+n1+n2+n3+n4)];
        fis=addmf(fis,'input',i,name,'gaussmf',[sig5(i) c5(j,i)]);
       end
   end
   %Add Output Membership Functions
   params=[ones(1,n1) 2*ones(1,n2) 3*ones(1,n3) 4*ones(1,n4) 5*ones(1,n5)];
   
   for i=1:num_rules
       name=['out_',num2str(i)];
       fis=addmf(fis,'output',1,name,'constant',params(i));
   end
   %Add FIS Rule Base
   ruleList=zeros(num_rules,m);
   for i=1:size(ruleList,1)
       ruleList(i,:)=i;
   end
   ruleList=[ruleList ones(num_rules,2)];
   
   fis=addrule(fis,ruleList);
end

