%% FUZZY SYSTEMS Project 4 Part A, August 2020

%  Author: Iason Georgios Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

% Other Files:
% --- split_scale.m: provided during the courseformat compact
% --- FIS_build.m: creates the FIS using subtractive clustering

%% Clear vars, Close figures, Create Folder to store variables
clear all;
close all;
dir=[pwd,'\images'];
mkdir(dir)

%% STEP 1: Load data, Split into Trn-Chk-Test, Preprocess it.
% Each column represents a feature of the dataset with the 4th column being the identifier
data = unique(load('haberman.data'),'rows');

%We need to check the proportion of each class identifier in each subset:
class=unique(data(:,end));
% CLASSES = 1,2

% % Firstly we will calculate the portions in the initial data and set max and min values
% % for these proportions allowing a variance of 0.5% around\
% % Having 289 instances, 0.5% means +-1.445 instances compared to the original
% 
% for i=1:2
%    p_min(i)=sum(data(:,end)==i)/size(data,1)-0.005;
%    p_max(i)=sum(data(:,end)==i)/size(data,1)+0.005;
% end
%    
% while 1
%    Data is split three-way based on the provided split-scale.m file.
%    [trnData,chkData,tstData]=split_scale(data,1);
%    for i=1:2
%       trnP(i)=sum(trnData(:,end)==i)/size(trnData,1);
%       chkP(i)=sum(chkData(:,end)==i)/size(chkData,1);
%       tstP(i)=sum(tstData(:,end)==i)/size(tstData,1);
%       cond1(i)=and(trnP(i)<p_max(i),trnP(i)>p_min(i));
%       cond2(i)=and(chkP(i)<p_max(i),chkP(i)>p_min(i));
%       cond3(i)=and(tstP(i)<p_max(i),tstP(i)>p_min(i));
%    end
%    if (cond1 & cond2 & cond3)
%       break;
%    end
% end

% % clear trnP chkP tstP cond1 cond2 cond3 p_min p_max i

% % Save the partitioning for traceability/validation reasons
%save('partitioning.mat','trnData','chkData','tstData')

% % If the same partitioning with the deliverable needs to be tested uncomment next line
% % and comment the previous 4 lines

load partitioning.mat


%% We will create and train 4 different models:
FIS=[];
trnFIS=[];
valFIS=[];
trnE=[];
valE=[];
model=[];
tStart=tic;

% The first two will be class independent
radius=[0.15 0.85];
ctr=1;
for r=radius
   % Subtractive Clustering - Class Independent
   [c,sig]=subclust(trnData,r);
   % Based on c (centers) and sig (influence per dimension), create manually the FIS
   fis=FIS_build(size(trnData,2),c,[],sig,[]);
   % Train the FIS
   [trnFis,trnError,~,valFis,valError]=anfis(trnData,fis,[600 0 0.01 0.9 1.1],[],chkData); 
   % Plot the Learning Curve of the training
   figure()
   plot([trnError,valError],'LineWidth',1);
   grid on;
   xlabel('# of Iterations'); ylabel('Error');
   legend('Training Error','Validation Error');
   title('Learning Curve');
   saveas(gcf,fullfile(dir,['C_Ind',num2str(r),'_learningcurve.png']))
   
   % Plot some fuzzy sets
for i=1:length(trnFis.Inputs) 
      figure() 
      subplot(2,1,1) 
      [x,mf] = plotmf(fis,'input',i); 
      plot(x,mf)
      title(['Initial MFs for Feature-Input ',num2str(i)])
      subplot(2,1,2) 
      [x,mf] = plotmf(valFis,'input',i); 
      plot(x,mf)
      title(['Tuned MFs for Feature-Input ',num2str(i)])
      saveas(gcf,fullfile(dir,['Model_',num2str(ctr),'_CompMfs_',num2str(i),'.png']))
end
   % Plot the representation of the clusters as spheres of respective radius and center
   figure()
   [x,y,z]=sphere();
   x=x*r; y=y*r; z=z*r;
      for i=1:size(c,1)
         hs=surf(x+c(i,1),y+c(i,2),z+c(i,3));
         set(hs, 'FaceAlpha', 0.5)
         hold on
         if c(i,4)==1
            set(hs,'FaceColor',	[0.6350, 0.0780, 0.1840])
         else
            set(hs,'FaceColor',  [0.4660, 0.6740, 0.1880])
         end    
      end
   hold off
   daspect([1 1 1])
   view(30,10)
   axis equal
   title('Clusters');
   saveas(gcf,fullfile(dir,['C_Ind',num2str(r),'_clusters.png']))
   % Store the model outcomes of training   
   my_model = struct('Type','ClassIndependent','radius',r);

   FIS=[FIS fis];
   trnFIS=[trnFIS trnFis];
   valFIS=[valFIS valFis];
   trnE=[trnE trnError];
   valE=[valE valError];
   model=[model my_model];
   ctr=ctr+1;
end

% The next two models will be class dependent (same radia)
for r=radius
   
   % Split data based on class (1 & 2), calculate centers and influence of each feature
   x=trnData(trnData(:,end)==1,:);     [c1,sig1]=subclust(x,r);
   x=trnData(trnData(:,end)==2,:);     [c2,sig2]=subclust(x,r);
   % Build FIS to be trained manually
   fis=FIS_build(size(trnData,2),c1,c2,sig1,sig2);
   % Train FIS
   [trnFis,trnError,~,valFis,valError]=anfis(trnData,fis,[600 0 0.01 0.9 1.1],[],chkData); 
   % Plot Learning Curve
   figure()
   plot([trnError,valError],'LineWidth',1);
   grid on;
   xlabel('# of Iterations'); ylabel('Error');
   legend('Training Error','Validation Error');
   title('Learning Curve');
   saveas(gcf,fullfile(dir,['C_Dep',num2str(r),'_learningcurve.png']))
      % Plot some fuzzy sets
for i=1:length(trnFis.Inputs) 
      figure() 
      subplot(2,1,1) 
      [x,mf] = plotmf(fis,'input',i); 
      plot(x,mf)
      title(['Initial MFs for Feature-Input ',num2str(i)])
      subplot(2,1,2) 
      [x,mf] = plotmf(valFis,'input',i); 
      plot(x,mf)
      title(['Tuned MFs for Feature-Input ',num2str(i)])
      saveas(gcf,fullfile(dir,['Model_',num2str(ctr),'_CompMfs_',num2str(i),'.png']))
end
   % Plot Cluster Representation
   figure()
   [x,y,z]=sphere();
   x=x*r; y=y*r; z=z*r;
   for i=1:size(c1,1)
      hs=surf(x+c1(i,1),y+c1(i,2),z+c1(i,3));
      set(hs, 'FaceAlpha', 0.5)
      hold on
      set(hs,'FaceColor',	[0.6350, 0.0780, 0.1840])
   end
   for i=1:size(c2,1)
      hs=surf(x+c2(i,1),y+c2(i,2),z+c2(i,3));
      set(hs, 'FaceAlpha', 0.5)
      hold on
      set(hs,'FaceColor',  [0.4660, 0.6740, 0.1880])
   end    
   hold off
   axis equal
   title('Clusters');
   saveas(gcf,fullfile(dir,['C_Dep',num2str(r),'_clusters.png']))
   
   % Store the model
   my_model = struct('Type','ClassDependent','radius',r);
   
   FIS=[FIS fis];
   trnFIS=[trnFIS trnFis];
   valFIS=[valFIS valFis];
   trnE=[trnE trnError];
   valE=[valE valError];
   model=[model my_model];
   ctr=ctr+1;
end
disp(['Training Done in ',num2str(toc(tStart)/60),' mins\n'])

%% We will evaluate these 4 different models and Print the results 
for i=1:4
   valFis=valFIS(i);
   % Calculate valFis's estimation
   tstY_hat=evalfis(tstData(:,1:end-1),valFis);
   % Round it and bring it on [1,2] (assuming estimations vary from [-.49, 3.49]
   % Otherwise we would have to modify the "+-1" factor below
   tstY_hat=round(tstY_hat)-1*double(tstY_hat>=2.5)+1*double(tstY_hat<0.5);
   % error Matrix
   eM=zeros(2,2);
   for k=1:size(tstData,1)
      eM(tstY_hat(k),tstData(k,end))= eM(tstY_hat(k),tstData(k,end))+1;
   end
   model(i).('Num_Rules')=length(valFis.Rule);
   model(i).('ErrorMatrix')=eM;
   model(i).('OverallAcc')=sum(diag(eM))/size(tstData,1); 
   model(i).('UserAcc')=diag(eM)./sum(eM,2);
   model(i).('ProducerAcc')=diag(eM)'./sum(eM,1);
   model(i).('k_hat')=(size(tstData,1)*sum(diag(eM))-sum(eM,1)*sum(eM,2))/(size(tstData,1)^2-sum(eM,1)*sum(eM,2));
end

fprintf("------------------------------------------------\n");
for i=1:4
   fprintf("Model %d evaluation\n",i)
   fprintf("Type: %s\n",model(i).Type)
   fprintf("Radius: %.2f\n",model(i).radius);
   fprintf("Number of Rules: %d\n",model(i).Num_Rules)
   disp('Error Matrix: ')
   disp(model(i).ErrorMatrix)
   disp('Overall Accuracy: ')
   disp(model(i).OverallAcc)
   disp('User Accuracy: ')
   disp(model(i).UserAcc)
   disp('Producer Accuracy: ')
   disp(model(i).ProducerAcc)
   disp('Kappa Coefficient: ')
   disp(model(i).k_hat)
   fprintf("------------------------------------------------\n");
end
save('models_A.mat','model')
