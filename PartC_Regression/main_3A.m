%% FUZZY SYSTEMS Project 3 Part A, August 2020

%  Author: Iason Georgios Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

% Other Files:
% --- split_scale.m: provided during the course
% --- airfoil_self_noise.dat: dataset to be used
% (Optionally) partitioning.mat: this has to be loaded in order to use the same
%              partitioning with the report.

%% Clear vars, Close figures, Create Folder to store variables
format compact
clear all;
close all;
dir=[pwd,'\images'];
mkdir(dir)

%% STEP 1: Load data, Split into Trn-Chk-Test, Preprocess it.
% Each column represents a feature of the dataset with the 6th column being the identifier
data = load('airfoil_self_noise.dat');

% Data is split three-way based on the provided split-scale.m file.
[trnData,chkData,tstData]=split_scale(data,1);
% Save the partitioning for traceability/validation reasons
save('partitioning.mat','trnData','chkData','tstData')

% If the same partitioning with the deliverable needs to be tested uncomment next line
% and comment the previous 4 lines

%load partitioning.mat

%% STEP 2: TSK Models
% Create a struct for our models with all the necessary info:
model(1) = struct ('no_of_MFs',2,'type_of_MFs','gbellmf','type_of_output','constant');
model(2) = struct ('no_of_MFs',3,'type_of_MFs','gbellmf','type_of_output','constant');
model(3) = struct ('no_of_MFs',2,'type_of_MFs','gbellmf','type_of_output','linear');
model(4) = struct ('no_of_MFs',3,'type_of_MFs','gbellmf','type_of_output','linear');

for k=1:4
   
   %% Model Creation and Training
   % Model Creation
   fis=genfis1(trnData,model(k).no_of_MFs,model(k).type_of_MFs,model(k).type_of_output);
   rulenumber(k)=length(fis.Rules);
   % Model Training. We will use valFis for the model that is created using validation Data.
   % Number of Epochs is arbitrarily set to 600. It is observed from some tests that
   % the learning curves tend to stabilize around 400 epochs.
   [trnFis,trnError,~,valFis,valError]=anfis(trnData,fis,[600 0 0.01 0.9 1.1],[],chkData);
   
   % The prediction of the model valFis on test dataset.
   tstY_hat=evalfis(tstData(:,1:end-1),valFis);
   
   %% Evaluate each Model
   se=sum((tstY_hat-tstData(:,end)).^2);
   ss=sum((mean(tstData(:,end))-tstData(:,end)).^2);
   model(k).('MSE')=se/size(tstData,1);
   model(k).('Rsquared')=1-se/ss;
   model(k).('RMSE')=sqrt(se/size(tstData,1));
   
   %% Plot Membership Functions
   for i=1:size(trnData,2)-1
      % Comparison between initial and tuned MFs
      figure() 
      subplot(2,1,1) 
      [x,mf] = plotmf(fis,'input',i); 
      plot(x,mf)
      title(['Initial MFs for Feature-Input ',num2str(i)])
      subplot(2,1,2) 
      [x,mf] = plotmf(valFis,'input',i); 
      plot(x,mf)
      title(['Tuned MFs for Feature-Input ',num2str(i)])
      saveas(gcf,fullfile(dir,['Model_',num2str(k),'_CompMfs_',num2str(i),'.png']))
      
      % Only tuned MFs
      figure()
      [x,mf] = plotmf(valFis,'input',i); 
      plot(x,mf)
      title(['Tuned MFs for Feature-Input ',num2str(i)])
      saveas(gcf,fullfile(dir,['Model_',num2str(k),'_TunedMFs_',num2str(i),'.png']))
   end
   
   %% Learning Curve and Output Estimation Plots
   figure()
   plot([trnError,valError],'LineWidth',1);
   grid on;
   xlabel('# of Iterations'); ylabel('Error');
   legend('Training Error','Validation Error');
   title('Learning Curve');
   saveas(gcf,fullfile(dir,['Model_',num2str(k),'_learningcurve.png']))

   figure()
   plot(1:size(tstData,1),tstData(:,end),'*r',1:size(tstData,1),tstY_hat, '.b');
   title('Estimation Error');
   legend('Reference Outputs','Model Outputs');
   saveas(gcf,fullfile(dir,['Model_',num2str(k),'_instances.png']))

end

%% MSE's and R^2 comparison of all models
figure()
bar([model(1).MSE,model(2).MSE,model(3).MSE,model(4).MSE])
xlabel('Model'); ylabel('MSE');
title('Evaluation Figure 1')
saveas(gcf,fullfile(dir,'ModelsComparisonMSE.png'))

figure()
bar([model(1).Rsquared,model(2).Rsquared,model(3).Rsquared,model(4).Rsquared])
xlabel('Model'); ylabel('Rsquared');
title('Evaluation Figure 2')
saveas(gcf,fullfile(dir,'ModelsComparisonR2.png'))

