%% FUZZY SYSTEMS Project 03 Part ?, August 2020

%  Author: Iason Georgios Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

% Other Files:
% --- split_scale.m: provided during the course
% --- train.csv: dataset to be used
% (Optionally) ranking.mat: in order to avoid running relieff algorithm again
%          and split.mat: to use the exact same division of datasets as in the report

%% Clear vars, Close figures
format compact
clear all;
close all;
clc
dir=[pwd,'\images2'];
mkdir(dir)

%% Load Data and Preprocess it 
data = unique(table2array(readtable('train.csv')),'rows');
m=size(data,2); %Number of Characteristics with the 82nd being the identifier.

% [trnData,chkData,tstData]=split_scale(data,1);
% 
% [ranking, ~] = relieff(data(:,1:end-1), data(:,end), 100);
% save('ranking.mat','ranking')
% save('split.mat','trnData','chkData','tstData')
load ranking.mat
load split.mat
model=[];

%% Initialize Grid Search Nodes
% The following lines have been adjusted iteratively, so the values differ. A total set is
% provided, but the user is encouraged to run FISs for fewer nodes, by creating similar
% vectors. "i-th value of the radius vector refers to i-th value of the features vector."

features=[ 6 6 8 8 13 13 13 13 17 17 17 17 20 20 20 20 20 25 25 25 25 25];
radius=[0.2 0.3 0.2 0.3 0.35 0.4 0.5 0.6 0.35 0.4 0.5 0.6 0.35 0.4 0.5 0.6 0.7 0.35 0.4 0.5 0.6 0.7];

%% Main FIS Training
for i=1:length(features)
    
    f=features(i); 
    r=radius(i);
   
    %Create a partitioning of the Training Data for 5-fold cross validation
    partition = cvpartition(trnData(:, end), 'KFold', 5);
    
    %Initialize the FIS
    fis = genfis2(trnData(:, ranking(1:f)), trnData(:, end), r);
    
    % If the rules generated are too many, then we will avoid training this FIS
    my_model = struct('no_of_features',f,'radius',r);
    meanTT=-1;
    meanError=-1;    
    if length(fis.Rule)<2
       my_model.('State')='Only 1 Rule';
    elseif length(fis.Rule)>200
       my_model.('State')='Over 200 Rules';
    else
       my_model.('State')='Accepted Model';
       meanTT=0;
       meanError=0;
       for k=1:5
          disp(['Model: ',num2str(f),' features, ',num2str(r),' radius, ', 'Fold ',num2str(k)])
          tstart=tic;  % Start calculating Training Time
          % Train the FIS
          [trnFis,trnError,~,valFis,valError] = anfis(trnData(partition.training(k),[ranking(1:f),m]), ...
             fis,[100 0 0.01 0.9 1.1],[],trnData(partition.test(k),[ranking(1:f),m]));
          % Calculate mean Training Time
          meanTT=meanTT+toc(tstart)/5;
          % Estimate the output of valFis on chkData
          tstY_hat=evalfis(valFis,chkData(:,ranking(1:f)));
          % Calculate mean Error
          meanError = meanError+sum((tstY_hat - chkData(:, end)) .^ 2)/size(chkData,1)/5;
       end       
       my_model.('meanTT')=meanTT;
       my_model.('meanError')=meanError;
       my_model.('noRules')=length(fis.Rule);
    end    
    % Store models
    model = [model my_model];
end
%% Save the models for further reference
save('model_tot.mat','model')
