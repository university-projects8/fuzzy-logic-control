%% FUZZY SYSTEMS Project 03 Part B Final, August 2020

%  Author: Iason Georgios Velentzas, AEM: 8785
%  mail:   velentzas@ece.auth.gr

%% Clear vars, Close figures
format compact
clear all;
close all;
clc
dir=[pwd,'\images2'];
mkdir(dir)

%% Load Data and Preprocess it 
data = unique(table2array(readtable('train.csv')),'rows');
m=size(data,2); %Number of Characteristics with the 82nd being the identifier.

[trnData,chkData,tstData]=split_scale(data,1);
load ranking.mat

%% Train the model with F=20 features and R=0.6 radius
F=20;
R=0.6;
my_model = struct('no_of_features',F,'radius',R);
fis = genfis2(trnData(:, ranking(1:F)), trnData(:, end), R);
[trnFis,trnError,~,valFis,valError] = anfis(trnData(:,[ranking(1:F),m]), fis,...
   [600 0 0.01 0.9 1.1],[],chkData(:,[ranking(1:F),m]));
tstY_hat=evalfis(valFis,tstData(:,ranking(1:F)));

   se=sum((tstY_hat-tstData(:,end)).^2);
   ss=sum((mean(tstData(:,end))-tstData(:,end)).^2);
   my_model.('MSE')=se/size(tstData,1);
   my_model.('Rsquared')=1-se/ss;
   my_model.('RMSE')=sqrt(se/size(tstData,1));

   
      %% Plot Membership Functions
      % Comparison between initial and tuned MFs
for i=[1,5,10,15,20]   
      figure() 
      subplot(2,1,1) 
      [x,mf] = plotmf(fis,'input',i); 
      plot(x,mf)
      title(['Initial MFs for Feature-Input ',num2str(i)])
      subplot(2,1,2) 
      [x,mf] = plotmf(valFis,'input',i); 
      plot(x,mf)
      title(['Tuned MFs for Feature-Input ',num2str(i)])
      saveas(gcf,fullfile(dir,['Model_','Final','_CompMfs_',num2str(i),'.png']))
end

      
      %% Learning Curve and Predictions
      figure()
      plot([trnError,valError],'LineWidth',1);
      grid on;
      xlabel('# of Iterations'); ylabel('Error');
      legend('Training Error','Validation Error');
      title('Learning Curve');
      saveas(gcf,fullfile(dir,'Learningcurve.png'))

      figure()
      plot(1:size(tstData,1),tstData(:,end),'*r',1:size(tstData,1),tstY_hat, '.b');
      title('Estimation Error');
      legend('Reference Outputs','Model Outputs');
      saveas(gcf,fullfile(dir,'Instances.png'))
