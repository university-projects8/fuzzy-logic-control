# Fuzzy Logic Control

## Part A: Control the angle of a Satellite using FLC

Consider the dynamical system which comprises of a unity feedback loop. The plant is given by the equation:

<img src="https://latex.codecogs.com/gif.latex?G_p(s) = \frac{10}{(s+1)(s+9)}" />

while the reference angle ranges between 0 and 60 degrees.

The scope of this assignment is to control the satellite with both a Fuzzy Controller and a Linear PI Controller and compare the output on specific scenarios.

#### Linear Controller
In order to achieve zero steady state error we use a PI Controller of the form:

<img src="https://latex.codecogs.com/gif.latex?G_c(s) = \frac{1K_p(s+c)}{s}, \; c = \frac{K_I}{K_p}" /> 

The controller must meet the following requirements:
- Overshoot less than 10% for step input
- Rise Time less than 1.2 sec.

For the controller design you should consider the following:
- The zero of the controller must be placed close to the system's dominant pole.  
- Try to satisfy the requirements by selecting an appropriate gain from the Root-Locus plot.

#### Fuzzy Controller
In order to achieve zero steady state error we use a FZ-PI Controller with a discrete timestep of 0.01 sec.

The membership functions of the signals, e, edot, udot can be seen below (on the factorized signals):

<p float=" left ">
  <img src="/PartA_Satellite/Diagrams/e_mf.png" width="33%" />
  <img src="/PartA_Satellite/Diagrams/edot_mf.png" width="33%" />
  <img src="/PartA_Satellite/Diagrams/udot_mf.png" width="33%" />
</p>

where the abbreviations stand for:
- N = Negative, P = Positive
- V = Very Large , L = Large, M = Medium, S = Small, ZR = Zero

The characteristics of the FLC are:
- Fuzzification Interface: Singleton
- AND Operator: min
- ALSO Operator: max
- Inference Mechanism: Larsen
- Defuzzifier Interface: COA


### Scenario 1
- Calculate the factorization gains so that the closed loop system response has better characteristics than the linear controller, namely: overshoot less than 7% and rise time less than 0.6 sec.

- Analytically calculate the Controller's output for inputs: (e, Δe) = (NS, ZR).

- Plot the output surface Δu based on inputs (e, Δe).

### Scenario 2
- Simulate the system's response for two different reference angles (u: step, r: ramp):

a)   <img src="https://latex.codecogs.com/gif.latex?ref(t) = 60u(t)-40u(t-4)+20u(t-8)" /> 

b)   <img src="https://latex.codecogs.com/gif.latex?ref(t) = 12r(t)-12r(t-5)-7.5r(t-8)+7.5r(t-16)" /> 

Draw conclusions about the ability of the Fuzzy Logic Controller to follow the step and the ramp reference signals.

## Part B: Control the movement of a Car using FLC

The purpose of this assignment is to control the 2-D movement of a car via a Fuzzy Controller. The car is located initially at _(x0,y0) = (4.1 , 0.3)_ and must be safely moved to _(xd,yd) = (10 , 3.2)_. It is required that the position error in y-axis is minimum. The obstacles are static and are the following rectangles: _obs1 = (5-6,0-1), obs2 = (6-7,0-2),  obs3 = (7-10,0-3)_.

The inputs of the controller are the _horizontal_ and _vertical distance_ of the above-mentioned obstacles, _dh_ and _dv_ ranging from 0 to 1 m, as well as the angle of the current velocity of the car, _θ_ ranging from -180 to 180 degrees. The output of the controller is the difference of this angle, namely _Δθ_, which can take values from -130 to 130 degrees.

The input and output of the controller are divided in fuzzy sets as shown below:
<img src="/PartB_Car/Diagrams/mf_init.png" width="70%" />


The characteristics of the FLC are can be summed up in the following scheme:

<img src="/PartB_Car/Diagrams/mamdani.png" width="70%" />

The defuzzifier interface is COA.

#### Scenario 1:
Simulate the movement of the car using various initial angle configurations and discuss the results.

#### Scenario 2:
Manually modify the rule base and the control range in order to achieve better results.

## Part C: Solve a Regression problem using TSK models

The purpose of this assignment is to investigate if TSK models are capable of modelling multivariable nonlinear functions. We will try to train fuzzy neural networks in order to estimate the target variable in two different UCI datasets. 

The first dataset is smaller and simpler, thus we will focus on the training and model evaluation process.

The second dataset is much more complex and we will delve into topics of dataset preprocessing (e.g. feature selection) and optimization methods (e.g. cross validation).

### Dataset 1
The first dataset is the [Airfoil Self-Noise Dataset](https://archive.ics.uci.edu/ml/datasets/airfoil+self-noise).

After splitting the dataset into _training, validation and check_ subsets and scaling the features we will train 4 different models:

| Name      | No. of Membership Functions | Output Format    |
| :---        |    :----:   |          ---: |
| TSK_Model_1      | 2       | Singleton  |
| TSK_Model_2   | 3        | Singleton      |
| TSK_Model_3      | 2       | Polynomial  |
| TSK_Model_4   | 3        | Polynomial   |

On all 4 models the membership functions are bell-shaped and the _hybrid method_ is used, which means that the membership functions' parameters are optimized through _backpropagation_ and the polynomial coefficients of the output are optimized through _LSA (Least Squares Algorithm)_. 

The metrics with which the models are evaluated are _MSE_, _Rsquared_, _NMSE_, _NDEI_ among the model's learning curve and complexity.

### Dataset 2
The first dataset is the [Superconductivty dataset](https://archive.ics.uci.edu/ml/datasets/Superconductivty+Data).

After splitting the dataset into _training, validation and check_ subsets we need to decide on the optimal number of features that will be used since 81 features would result in 2^81 IF-THEN rules. Hence the grid selection method is employed and the results for each "n" (number of features) are verified via 5-fold cross-validation.

The axes of the grid are the number of features "n" and the clusters' radius "r".

After we select the appropriate point of the grid (which in order to increase the efficacy and efficiency of the search will be done employed recursively) we will train the selected model on the "n" most important features of it. In order to decide the most important features the Relief Algorithm is employed. The clusters are created via the Subtractive Clustering Algorithm.


The metrics with which the models are evaluated are _MSE_, _Rsquared_, _NMSE_, _NDEI_ among the model's learning curve and complexity.

## Part D: Solve a Classification problem using TSK models

The purpose of this assignment is to investigate if TSK models are capable of tackling classification problems. We will try to train fuzzy neural networks in order to estimate the target variable in two different UCI datasets. 

The first dataset is smaller and simpler, thus we will focus on the training and model evaluation process.

The second dataset is much more complex and we will delve into topics of dataset preprocessing (e.g. feature selection) and optimization methods (e.g. cross validation).

### Dataset 1
The first dataset is the [Haberman's Survival Dataset](https://archive.ics.uci.edu/ml/datasets/haberman's+survival).

After splitting the dataset into _training, validation and check_ subsets and scaling the features we will train 4 different models:

| Name      | Clustering Type| Cluster Radius    |
| :---        |    :----:   |          ---: |
| TSK_Model_1      | Class Independent       | Extremely Low  |
| TSK_Model_2   | Class Independent   | Extremely High      |
| TSK_Model_3      | Class Dependent       | Extremely Low  |
| TSK_Model_4   | Class Dependent         | Extremely High   |

On all 4 models the membership functions are bell-shaped and the _hybrid method_ is used, which means that the membership functions' parameters are optimized through _backpropagation_ and the polynomial coefficients of the output are optimized through _LSA (Least Squares Algorithm)_. It is also noted that the output of the model should be discretized and the method is left in the designer's _discretion_.

The metrics with which the models are evaluated are _Error Matrix_, _Overall Accuracy_, _Producer's/User's Accuracy_, _Khat_ among the complexity and learning curve of each model.

### Dataset 2
The first dataset is the [Epileptic Seizure Recognition dataset](https://archive.ics.uci.edu/ml/datasets/Epileptic+Seizure+Recognition).

After splitting the dataset into _training, validation and check_ subsets we need to decide on the optimal number of features that will be used since 179 features would result in _an infeasible number_ of rules. Hence the grid selection method is employed and the results for each "n" (number of features) are verified via 5-fold cross-validation.

The axes of the grid are the number of features "n" and the clusters' radius "r".

After we select the appropriate point of the grid (which in order to increase the efficacy and efficiency of the search will be done employed recursively) we will train the selected model on the "n" most important features of it. In order to decide the most important features the Relief Algorithm is employed. The clusters are created via the Subtractive Clustering Algorithm and is employed separately for each class of the dataset.

The metrics with which the models are evaluated are _MSE_, _Rsquared_, _NMSE_, _NDEI_ among the model's learning curve and complexity.


# Results and Simulation Diagrams

## Part A:

#### Fuzzy Rule Base
<img src="/PartA_Satellite/Diagrams/rules.png" width="60%" />

#### Fuzzy and Linear Comparison
<p float=" left ">
  <img src="/PartA_Satellite/Diagrams/f_l_i.png" width="40%" />
  <img src="/PartA_Satellite/Diagrams/f_l_m.png" width="40%" />
</p>

#### Output Surface
<p float=" left ">
  <img src="/PartA_Satellite/Diagrams/3d_1.png" width="40%" />
  <img src="/PartA_Satellite/Diagrams/3d_2.png" width="40%" />
</p>

#### Step and Ramp Reference Signals

<p float=" left ">
  <img src="/PartA_Satellite/Diagrams/step.png" width="40%" />
  <img src="/PartA_Satellite/Diagrams/ramp.png" width="40%" />
</p>

## Part B:

#### Rule Base
<img src="/PartB_Car/Diagrams/rule_base.png" width="60%" />

#### Initial Simulations
<p float=" left ">
  <img src="/PartB_Car/Diagrams/traj_init_0.png" width="33%" />
  <img src="/PartB_Car/Diagrams/traj_init_45.png" width="33%" />
  <img src="/PartB_Car/Diagrams/traj_init_90.png" width="33%" />
</p>


#### Modifications
<img src="/PartB_Car/Diagrams/mf_fin.png" width="70%" />

#### Final Simulations

<p float=" left ">
  <img src="/PartB_Car/Diagrams/traj_fin_0.png" width="33%" />
  <img src="/PartB_Car/Diagrams/traj_fin_45.png" width="33%" />
  <img src="/PartB_Car/Diagrams/traj_fin_90.png" width="33%" />
</p>

## Part C:

#### Learning Curves
<p float=" left ">
  <img src="/PartC_Regression/Diagrams/lc_1.png" width="24.5%" />
  <img src="/PartC_Regression/Diagrams/lc_2.png" width="24.5%" />
  <img src="/PartC_Regression/Diagrams/lc_3.png" width="24.5%" />
  <img src="/PartC_Regression/Diagrams/lc_4.png" width="24.5%" />
</p>

#### Prediction Errors
<p float=" left ">
  <img src="/PartC_Regression/Diagrams/pe_1.png" width="24.5%" />
  <img src="/PartC_Regression/Diagrams/pe_2.png" width="24.5%" />
  <img src="/PartC_Regression/Diagrams/pe_3.png" width="24.5%" />
  <img src="/PartC_Regression/Diagrams/pe_4.png" width="24.5%" />
</p>

#### Model Evaluation Metrics
<img src="/PartC_Regression/Diagrams/res_1.png" width="50%" />

#### Grid Search Recursive
<p float=" left ">
  <img src="/PartC_Regression/Diagrams/gs_1.png" width="33%" />
  <img src="/PartC_Regression/Diagrams/gs_2.png" width="33%" />
  <img src="/PartC_Regression/Diagrams/gs_3.png" width="33%" />
</p>

#### Grid Search Evaluation
<p float=" left ">
  <img src="/PartC_Regression/Diagrams/gs_4.png" width="40%" />
  <img src="/PartC_Regression/Diagrams/gs_5.png" width="40%" />
</p>

#### Feature Selection
<img src="/PartC_Regression/Diagrams/res_2.png" width="50%" />


#### Final Model Training
<p float=" left ">
  <img src="/PartC_Regression/Diagrams/ee_1.png" width="40%" />
  <img src="/PartC_Regression/Diagrams/lc_fin.png" width="40%" />
</p>

#### Final Model Evaluation Metrics
<img src="/PartC_Regression/Diagrams/res_2.png" width="50%" />

## Part D

#### Models
<p float=" left ">
  <img src="/PartD_Classification/Diagrams/model_1.png" width="40%" />
  <img src="/PartD_Classification/Diagrams/model_2.png" width="40%" />
</p>
<p float=" left ">
  <img src="/PartD_Classification/Diagrams/model_3.png" width="40%" />
  <img src="/PartD_Classification/Diagrams/model_4.png" width="40%" />
</p>

#### Metrics

<img src="/PartD_Classification/Diagrams/met_1.png" width="50%" />
<img src="/PartD_Classification/Diagrams/met_2.png" width="50%" />
<img src="/PartD_Classification/Diagrams/met_3.png" width="50%" />
<img src="/PartD_Classification/Diagrams/met_4.png" width="50%" />

#### Grid Search Recursive
<p float=" left ">
  <img src="/PartD_Classification/Diagrams/gs_1.png" width="25%" />
  <img src="/PartD_Classification/Diagrams/gs_2.png" width="34%" />
  <img src="/PartD_Classification/Diagrams/gs_3.png" width="40%" />
</p>


#### Final Model Training
<p float=" left ">
  <img src="/PartD_Classification/Diagrams/ee_disc.png" width="40%" />
  <img src="/PartD_Classification/Diagrams/ee_real.png" width="40%" />
</p>

<img src="/PartD_Classification/Diagrams/lc_fin.png" width="50%" />

#### Final Model Evaluation Metrics
<img src="/PartD_Classification/Diagrams/met_fin.png" width="50%" />


# FILES DESCRIPTION

For any of the below mentioned mat files please contact the author @ iason.g.velentzas@gmail.com

#### PART A

        -Part 1
        The first part of the assignment referring to the Design, is implemented
        in the main01.m script. The Linear PI Controller is designed to meet some
        specific requirements. The iterative process for gain selection
        was manual, so it is not included.

        In the script myFLC_01.m the FLC is created (membership functions and rulebase).
        This file is called in main01.m so as to plot the membership functions.

        -Part 2
        The second part of the assignment contains the actual implementation of
        some scenarios. The first one is being studied extensively and the second
        one is only commented on its results. These scenarios are created using
        SimuLink. In order to inspect the properties of the responses we will use
        the Scope block, in which the desired outputs are connected. So, this part
        of the assignment will not have interaction with the main script, but
        everything will be set manually inside the SimuLink Model.

        After one opens the 'Simulations.slx' Model, one can see 4 different Blocks
        and also a Switch at the top of the Model, which functions as a Selector. By
        defining the Constant, we select which of the 4 different reference signals
        will be provided to all the blocks. The correspondence is also shown to the 
        SimuLink Model. The 4 different references signals correspond to:

        1) Scenario 1, r(t)=10;
        2) Scenario 1, r(t)=50;
        3) Scenario 2, profile=1 (steps);
        4) Scenario 2, profile=2 (ramps);

        For each different block there is a Scope in which we can inspect all properties.

        The User is encouraged to define the simulation time (currently set to 18 secs).
        For scenario 1, appropriate time is 5 or 10 secs and for scnenario 2, 16 or 18 secs.

        After having selected simulation Time and a value for the selector, it can open any
        Scope of preference and display Bilevel Measurements.

#### Part B

        This whole assignment can be run through modifying the main_02.m script.
        It contains the initializations for the three different initial orientations
        the vehicle will simulate. Each run of the script generates the Membership
        Functions of the implented FLC and these three simulations.

        It makes reference to two class files: Point.m and Vehicle.m which have been
        created for the purposes of this project.
        --Point.m 	represents a point in the grid and is used to validate the input data
              and the simulation.
              Now that the FLC is created and works properly it seems useless, but
              during the design process, it helped identifying easily the reason of
              an error.
        --Vehicle.m represents the Vehicle object. It was also used for debug purposes, but
              this one has actual use in the code. It contains the function that
              replace the measurement process of the sensors. It has the functions:
              --get_sensors_dv(Obj) and
              --get_sensors_dh(Obj)
              
        Lastly, we have the FLC generation script, "myFLC.m". This takes an argument, which
        indicates which of the FLCs designed will be implemented. For this choice, the User
        is asked to comment/uncomment the following lines in main_02.m script:

        %Line 43% flc_system=myFLC(0);		//This is the initial FLC  
        %Line 44% flc_system=myFLC(1);		//This is the enhanced FLC

        Selecting one of these two lines will generate the respective simulations.

        Lastly, if one wants to validate the figures at the end of the report, it can easily
        modify myFLC.m file in order for the output range not be increased, and the output MFs
        having the initial range.


#### Part C

        -Part 1

        For this part we will use the file "airfoil_self_noise.dat". One only has to run
        main_3A.m script in order for the 4 different models to be created and trained.
        In order for the results in the report to be validable, we provide among this script
        the "split.mat" file, which contains the exact same split used to train the FISs.

        Among this, we also provide "ranking.mat" which contains the result of the relieff
        algorithm. The purpose of it is to save the user some time on the next part. 
        Relieff algorithm is independent from data split, so it will always result in the 
        same ranking.

        Currently, in main_3A.m script, we have uncommented the options of loading split.mat,
        instead of these generating them. Should the user desire to generate
        the models on a different dataset split, it is encouraged to just uncomment and comment
        the respective lines in Section 2 of the script. For this, the file split_scale.m is
        necessary (provided in the Courses' Lab Section).

        The plots that will be generated, will be saved in the current path in a file "images".

        -Part 2

        For this part we will use the file "train.csv", containing the Superconductivity dataset.
        Similarly to part A, we will need to run main_3B.m script for the 'Grid Search' and 
        'Cross-Validation' part of the process. 
        We use the same .mat files as in Part A: "ranking.mat" and "split.mat". The model
        results during the process were stored in .mat files, which are also provided for 
        validation purposes. Namely we have: model_1.mat, model_2.mat, model_3.mat and 
        model_tot.mat (for each repetition of the search). Currently, we have created a total
        vector of features and radia for the User, which will be stored in a model_tot.mat file.

        There are no plots generated and the diagrams in the report were created manually.

        For the selected model one has to run final_TSK_model.m file. Accordingly with main_3A there
        are the same plots, same inputs (ranking.mat and split.mat) and same evaluation metrics.
        The FIS generation and training though, is similar to main_3B.m. The plots are saved in
        the current path in a folder named "images2".

#### Part D

        -Part 1

        For this part we will use the file "haberman.data". One only has to run
        main_4A.m script in order for the 4 different models to be created and trained.
        In order for the results in the report to be validable, we provide among this script
        the "partitioning.mat" file, which contains the exact same partitioning used to 
        train the FISs.

        Currently, in main_4A.m script, we have uncommented the options of loading split.mat,
        instead of these generating them. Should the user desire to generate
        the models on a different dataset split, it is encouraged to just uncomment and comment
        the respective lines in Section 2 of the script. For this, the file split_scale.m is
        necessary (provided in the Courses' Lab Section).

        Another file that is referenced in main is FIS_build. This file takes as inputs the total
        number of testdata, centers and significane (outcomes of subtractive clustering) and 
        creates manually the FIS to be trained. It  is highly influenced by another script provided
        in the Lab section of the course "TSK_Classification.m".

        Running main_4A.m will generate the models and their evaluation, plot the clusters and also
        plot the Learning Curves of each model.

        -Part 2

        For this part we will use the file "epilleptic.csv", containing the Epilleptic Seizure
        Recognition dataset. Similarly to part A, we will need to run main_3B.m script for the 
        'Grid Search' and 'Cross-Validation' part of the process. 
        Here we use a partitioning2.mat file for the partition of dataset and ranking.m for the
        output of relief algorithm. If the user wants to run again with a different split, it is
        encouraged to uncomment and comment the respective lines.
        For validation purposes there are also the files model_A.mat and model_B.mat provided,
        which contain the characteristics of the models/nodes examined.
        There are no plots generated since, the diagrams in the report are created manually.

        For the selected model one has to run final_TSK_model.m file. Accordingly with main_4A there
        are the same plots, same inputs (ranking.mat and partitioning2.mat) and same evaluation metrics.
        The FIS generation and training though, is similar to main_4B.m. The plots are saved in
        the current path in a folder named "images2".
